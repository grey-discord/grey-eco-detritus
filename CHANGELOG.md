# Version 0.1.0 (Current Version - Beta)
- Started project

- Added 4 new categories:
	- Utility
	- Shop
	- Fun
	- Economy

- Added 12 new commands:
	- Balance (Economy)
	- Pay (Economy)
	- SetMoney (Economy)
	- Snipe (Fun)
	- Shop.NewItem/Shop NewItem (Shop)
	- Shop.SetEmoji/Shop SetEmoji (Shop)
	- Shop (Shop)
	- Embed (Utility)
	- Eval (Utility)
	- Help (Utility)
	- Ping (Utility)
	- Reload (Utility)

- And more...