import { Colors, CommandMetadata } from "@src/interfaces";
import { Command, CommandClient } from "detritus-client";
import type { Context, ParsedArgs } from "detritus-client/lib/command";
import { Permissions } from "detritus-client/lib/constants";
import { Embed, Markup } from "detritus-client/lib/utils";

interface CommandMetadataOptions extends Command.CommandOptions {
  metadata: CommandMetadata
}

export class BaseCommand<ParsedArgsFinished = Command.ParsedArgs> extends Command.Command<ParsedArgsFinished> {
    triggerTypingAfter = 1000;

    constructor(client: CommandClient, options: Partial<CommandMetadataOptions>) {
      super(client, Object.assign({
        name: '',
        permissionsClient: [Permissions.EMBED_LINKS, Permissions.SEND_MESSAGES]
      }, <CommandMetadataOptions>options))
    }

    onBefore(context: Context) {
      if(!context.message.inDm && !['504440862844125204', '841117982076043264'].includes(context.guildId!)) {
        const embed = new Embed()
        .setTitle('➜ Command Failed')
        .setColor(Colors.RED)
        .setDescription(Markup.bold('This bot is available in certain servers only.'))
        context.editOrReply({ embed: embed, reference: true })
        return false;
      }
      
      if((context.command?.metadata as CommandMetadata).nsfw === true && context.channel?.nsfw === false) {
        const embed = new Embed()
        .setTitle('➜ Command Failed')
        .setColor(Colors.RED)
        .setDescription(Markup.bold('Please run this command in an NSFW channel.'))
        context.editOrReply({ embed: embed, reference: true })
        return false;
      }

      if((context.command?.metadata as CommandMetadata).adminOnly === true && !context.commandClient.settings.config?.operators.includes(context.userId)) {
        const embed = new Embed()
        .setTitle('➜ Command Failed')
        .setColor(Colors.RED)
        .setDescription(Markup.bold('This is a client operators only command.'))
        context.editOrReply({ embed: embed, reference: true })
        return false;
      }
      return true;
    }

    onTypeError(context: Context, _args: Array<any>, errors: Error[]) {
      const description = ['ERRORS'];
      for (let key in errors) {
        description.push(`**${key}**: ${errors[key].message}`);
      }
      return context.editOrReply(description.join('\n'));
    }
  }