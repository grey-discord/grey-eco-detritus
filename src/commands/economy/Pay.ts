import type { Context, ParsedArgs } from "detritus-client/lib/command";
import { BaseCommand } from "@src/commands/BaseCommand";
import { Economy, Logger } from "@src/classes";
import { Colors, Constants } from "@src/interfaces";
import { CommandClient, Structures } from "detritus-client";
import { Embed, Markup, regex } from "detritus-client/lib/utils";
import { DiscordRegexNames, Snowflake } from "detritus-client/lib/constants";

interface SpecialArgs extends ParsedArgs {
    user: Structures.User,
    amount: number
}

function isSnowflake(value: string|number): value is Snowflake {
    if (16 <= value.toString().length && value.toString().length <= 21) {
      return !!parseInt(value.toString());
    }
    return false;
}

export = class SetMoney extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'pay',
            metadata: {
                category: 'ECONOMY',
                description: 'Pay a user with Grecoins.',
                adminOnly: true
            },
            type: [
                { 
                    name: 'user',
                    label: 'user',
                    default: (context: Context) => context.user,
                    type: (value: string, context: Context) => {
                        if(value) {
                            return Promise.resolve((async () => {
                                try {
                                        {
                                            const { matches } = regex(DiscordRegexNames.MENTION_USER, value) as {matches: Array<{id: string}>};
                                            if(matches.length) {
                                                const { id: userId } = matches[0];
                                                if(isSnowflake(userId)) {
                                                    value = userId
                                                }
                                        }

                                            if(isSnowflake(value)) {
                                                return context.users.get(value)
                                            }
                                        }
                                } catch (err) {
                                    new Logger().error(err)
                                }

                                return null;
                            })())
                        }
                    }
                }, 
                {
                    name: 'amount',
                    label: 'amount',
                    type: 'NUMBER',
                }
            ]
        })
    }

    onBeforeRun(context: Context, args: SpecialArgs) {
        return !!args.user && !!args.amount && args.user.id !!== context.userId
    }

    onCancelRun(context: Context, args: SpecialArgs) {
        if(!args.user || !isSnowflake(args.user.id || '')) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('This user cannot be found.')).setTimestamp(), reference: true })
            return false;
        }
        if(!args.amount) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Please enter a valid amount (number).')).setTimestamp(), reference: true })
            return false;
        }
        if(args.user.id === context.userId) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('You cannot pay yourself.')).setTimestamp(), reference: true })
            return false;
        }
        if(args.amount > new Economy(context).getBalance(context.guildId!, context.userId)) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('You do not have enough balance to pay ' + args.user.username)).setTimestamp(), reference: true })
            return false;
        }
        return true;
    }


    async run(context: Context, args: SpecialArgs) {
        let user = context.users.get(args.user.id)
        let client = new Economy(context)
        let balance = client.getBalance(context.guildId!, args.user.id)
        await client.setBalance(context.guildId!, context.userId, (client.getBalance(context.guildId!, context.userId) - args.amount))
        await client.setBalance(context.guildId!, user!.id, +(balance += args.amount))
        return context.editOrReply({ embed: new Embed()
            .setTitle('➜ Payed Money')
            .setColor(Colors.ORANGE)
            .setDescription(Markup.bold(`Your new balance is ${Constants.coin_emoji} ${client.getBalance(context.guildId!, context.userId)} ${Constants.coin_name}.`)), reference: true })
    }
}
