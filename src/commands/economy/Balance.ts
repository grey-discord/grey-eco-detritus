import type { Context, ParsedArgs } from "detritus-client/lib/command";
import { BaseCommand } from "@src/commands/BaseCommand";
import { Economy, Logger } from "@src/classes";
import { Colors, Constants } from "@src/interfaces";
import { CommandClient, Structures } from "detritus-client";
import { Embed, Markup, regex } from "detritus-client/lib/utils";
import { DiscordRegexNames, Snowflake } from "detritus-client/lib/constants";

interface SpecialArgs extends ParsedArgs {
    user: Structures.User
}

function isSnowflake(value: string|number): value is Snowflake {
    if (16 <= value.toString().length && value.toString().length <= 21) {
      return !!parseInt(value.toString());
    }
    return false;
}

export = class Balance extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            label: 'user',
            name: 'balance',
            metadata: {
                category: 'ECONOMY',
                description: 'View your/a user\'s balance of Grecoins.'
            },
            aliases: ['bal'],
            default: (context: Context) => context.user,
            type: (value: string, context: Context) => {
                if(value) {
                    return Promise.resolve((async () => {
                        try {
                            const { matches } = regex(DiscordRegexNames.MENTION_USER, value) as {matches: Array<{id: string}>};
                                if(matches.length) {
                                    const { id: userId } = matches[0];
                                    if(isSnowflake(userId)) {
                                        value = userId
                                    }
                                }

                                if(isSnowflake(value)) {
                                    return context.users.find(u => u.id === value)!
                                }          
                        } catch (err) {
                            new Logger().error(err)
                        }

                        return null;
                    })())
                }
            }
        })
    }

    onBeforeRun(_context: Context, args: SpecialArgs) {
        return !!args.user
    }

    onCancelRun(context: Context, _args: SpecialArgs) {
        context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('This user cannot be found.')).setTimestamp(), reference: true })
        return true;
    }

    run(context: Context, args: SpecialArgs) {
        let user = context.users.get(args.user.id)
        let balance = new Economy(context).getBalance(context.guild!.id, user!.id) || 0
        return context.editOrReply({ embed: new Embed()
            .setTitle('➜ Balance')
            .setColor(Colors.ORANGE)
            .setDescription(Markup.bold(`${user!.id === context.message.author.id ? 'Your' : `${user!.username}'s`} balance is ${Constants.coin_emoji} ${balance} ${Constants.coin_name}.`)), reference: true })
    }
}
