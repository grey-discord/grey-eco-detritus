import { Context, ParsedArgs } from "detritus-client/lib/command";
import type { CommandClient } from "detritus-client";
import { BaseCommand } from "@src/commands/BaseCommand";
import { ShopsManager } from "@src/classes";
import { Colors } from "@src/interfaces";
import { ComponentEmojiData, Embed, Markup } from "detritus-client/lib/utils";

interface SpecialArgs extends ParsedArgs {
    name: string,
    price: number,
    emoji?: ComponentEmojiData,
}

export = class Shop extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'shop newitem',
            aliases: ['shop new'],
            metadata: {
                category: 'SHOP',
                description: 'Create a new item for GreyEco\'s shop.',
                adminOnly: true,
                usage: `ge?shop newitem <price> <name>`,
                examples: [
                    `ge?shop newitem 10 Sky's Bath Water`
                ]
            },
            type: [
                {
                    name: 'price',
                    label: 'price',
                    type: Number
                },
                {
                    name: 'name',
                    label: 'name',
                    type: 'STRING',
                    consume: true
                }
            ]
        })
    }

    onBeforeRun(context: Context, args: SpecialArgs) {
        return !!args.price && !!args.name && new ShopsManager(context).hasItemWithName(args.name.toLowerCase()) !== true
    }

    onCancelRun(context: Context, args: SpecialArgs) {
        if(!args.price || isNaN(args.price)) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Please enter a valid price (number).')).setTimestamp(), reference: true })
            return false;
        }
        if(!args.name) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Please enter a item name.')).setTimestamp(), reference: true })
            return false;
        }
        if(new ShopsManager(context).hasItemWithName(args.name.toLowerCase())) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Item already exists.')).setTimestamp(), reference: true })
            return false;
        }
        return true;
    }
    
    run(context: Context, args: SpecialArgs) {
        let shop = new ShopsManager(context)
        shop.newItem(args.name, args.price, undefined, 'No description defined.')
        let embed = new Embed()
        .setTitle('➜ New Item')
        .setColor(Colors.ORANGE)
        .setDescription(`Created item with ID ${Markup.codestring(shop.getID(args.name) || 'unknown')}`)
        context.editOrReply({ embed: embed, reference: true })
    }
}