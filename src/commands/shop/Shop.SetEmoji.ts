import { Context, ParsedArgs } from "detritus-client/lib/command";
import type { CommandClient } from "detritus-client";
import { BaseCommand } from "@src/commands/BaseCommand";
import { ShopsManager } from "@src/classes";
import { Colors } from "@src/interfaces";
import { ComponentEmojiData, Embed, Markup } from "detritus-client/lib/utils";

interface SpecialArgs extends ParsedArgs {
    name: string,
    emoji: ComponentEmojiData | string
}

export = class Shop extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'shop setemoji',
            metadata: {
                category: 'SHOP',
                description: 'Set the emoji for an item displayed in GreyEco\'s shop.',
                adminOnly: true,
                examples: [
                    `ge?shop setemoji 10 Sky's Bath Water`
                ]
            },
            type: [
                {
                    name: 'name',
                    label: 'name',
                    type: 'STRING',
                },
                {
                    name: 'emoji',
                    label: 'emoji',
                    type: 'STRING'
                }
            ]
        })
    }

    onBeforeRun(context: Context, args: SpecialArgs) {
        return !!args.name && !!args.emoji && !!new ShopsManager(context).hasItemWithID(args.name.toLowerCase())
    }

    onCancelRun(context: Context, args: SpecialArgs) {
        if(!args.name) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Please enter an item ID.')).setTimestamp(), reference: true })
            return false;
        }
        if(!new ShopsManager(context).hasItemWithID(args.name.toLowerCase())) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Item does not exist.')).setTimestamp(), reference: true })
            return false;
        }
        if(!args.emoji) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Please enter a valid emoji.')).setTimestamp(), reference: true })
            return false;
        }
        return true;
    }
    
    run(context: Context, args: SpecialArgs) {
        console.log(args)
        let shop = new ShopsManager(context)
        shop.setEmoji(args.name, args.emoji.toString())
        let embed = new Embed()
        .setTitle('➜ Set Item Emoji')
        .setColor(Colors.ORANGE)
        .setDescription(`Set emoji "${args.emoji}" for item with ID ${Markup.codestring(args.name)}`)
        context.editOrReply({ embed: embed, reference: true })
    }
}