import type { Context, ParsedArgs } from "detritus-client/lib/command";
import type { CommandClient } from "detritus-client";
import { BaseCommand } from "@src/commands/BaseCommand";
import { ShopsManager } from "@src/classes";
import { Colors, Constants } from "@src/interfaces";
import { Embed, Markup } from "detritus-client/lib/utils";

export = class Shop extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'shop',
            metadata: {
                category: 'SHOP',
                description: 'View GreyEco\'s shop.'
            },
        })
    }
    
    run(context: Context, _args: ParsedArgs) {
        let shop = new ShopsManager(context).collection()
        let embed = new Embed().setTitle('➜ Shop').setColor(Colors.ORANGE)
        let data = ''
        shop?.forEach(item => {
            if(!item || !shop!.length) return data += '**No items.**'
            data += `${Markup.bold(`${item.emoji.toString().trim()} \|\| ${item.name}`)}\n${Markup.italics(`${item.description || 'No description.'}`)}\n${Constants.coin_emoji} ${item.price} ${Constants.coin_name}\nID: ${Markup.codestring(new ShopsManager(context).getID(item.name)!)}\n\n`
        })
        embed.setDescription(data)
        return context.editOrReply({ embed: embed, reference: true })
    }
}
