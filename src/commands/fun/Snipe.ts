import type { Context, ParsedArgs } from "detritus-client/lib/command";
import type { CommandClient } from "detritus-client";
import type { Snowflake } from "detritus-client/lib/constants";
import { BaseCommand } from "@src/commands/BaseCommand";
import { Colors } from "@src/interfaces";
import { Embed, Markup } from "detritus-client/lib/utils";

export = class Snipe extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'snipe',
            metadata: {
                category: 'FUN',
                description: 'Snipes latest deleted message (or image)'
            }
        });
    }

    run(context: Context, _args: ParsedArgs) {
        let data:{
            message: string,
            author: Snowflake,
            attachment?: string,
            date: number,
        } = context.commandClient.settings.collections.sniped.get(context.messageId)

        if(!data || !data.message) {
            let embed = new Embed()
            .setTitle('➜ Snipe')
            .setColor(Colors.ORANGE)
            .setDescription(Markup.bold('No sniped messages or images.'))
            return context.editOrReply({ embed: embed, reference: true })
        }

        let embed = new Embed()
        .setTitle('➜ Snipe')
        .setColor(Colors.ORANGE)
        .setDescription(data.message || null)
        .setAuthor(context.users.get(data.author.toString())!.username, context.users.get(data.author.toString())!.defaultAvatarUrl)
        .setTimestamp(data.date)

        if(data.attachment) embed.setImage(data.attachment)
        return context.editOrReply({ embed: embed, reference: true })
    }
}
