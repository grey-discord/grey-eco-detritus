import { CommandClient, Constants, Utils } from "detritus-client";
import type { Context, ParsedArgs } from "detritus-client/lib/command";
import { Embed } from "detritus-client/lib/utils";
import { BaseCommand } from "@src/commands/BaseCommand";
import { Colors } from "@src/interfaces";

export = class Eval extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            label: 'code',
            name: 'eval',
            args: [
                {   
                    label: 'async',
                    name: '-async',
                    aliases: ['-a'],
                    type: 'bool',
                    default: false
                },
                {   
                    label: 'silent',
                    name: '-silent',
                    aliases: ['-s'],
                    type: 'bool',
                    default: false
                },
                {   
                    label: 'typescript',
                    name: '-typescript',
                    aliases: ['-t'],
                    type: 'bool',
                    default: false
                }
            ],
            metadata: {
              category: 'UTILITY',
              adminOnly: true
            }
        })
    }

    async run(context: Context, args: ParsedArgs) {
      try {
        const { matches } = Utils.regex(Constants.DiscordRegexNames.TEXT_CODEBLOCK, args.code);
        if (matches.length) {
          args.code = matches[0].text;
        }

        if(args.async) args.code = `(async () => { ${args.code} })();`
    
        let language = 'js';
        let message;
        try {
          message = await Promise.resolve(eval(args.code));
          if (typeof(message) === 'object') {
            message = JSON.stringify(message, null, 2);
            language = 'json';
          }
        } catch(error) {
          message = (error) ? error.stack || error.message : error;
        }

        const max = 1990 - language.length;

        let embed = new Embed()
        .setTitle('➜ Eval')
        .setColor(Colors.ORANGE)
        .setDescription(['```' + language, String(message).slice(0, max), '```'].join('\n'))

        if(args.async) embed.setFooter('Asynchronized', context.client.user?.avatarUrl)
        if(args.typescript) embed.setFooter('Compiled TypeScript', context.client.user?.avatarUrl)
        if(args.async && args.typescript) embed.setFooter('Asynchronized Compiled Typescript', context.client.user?.avatarUrl)
    
        if (!args.silent) {
          return context.editOrReply({ embed: embed, reference: true });
        }
      } catch (err) {
        console.log(err)
      }
    }
}