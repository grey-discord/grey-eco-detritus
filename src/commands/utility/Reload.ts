import { CommandClient } from "detritus-client";
import type { Context, ParsedArgs } from "detritus-client/lib/command";
import { Embed, Markup } from "detritus-client/lib/utils";
import { BaseCommand } from "@src/commands/BaseCommand";
import { Colors } from "@src/interfaces";
import { Logger } from "@src/classes";

export = class Reload extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'reload',
            metadata: {
              description: 'Reloads all commands.',
              category: 'UTILITY',
              adminOnly: true
            },
        })
    }

    onBeforeRun(context: Context, _args: ParsedArgs) {
        return context.commandClient.settings.config?.operators.includes(context.userId)!
    }

    onCancelRun(context: Context, _args: ParsedArgs) {
      if(!context.commandClient.settings.config?.operators.includes(context.userId)) {
        const embed = new Embed()
        .setTitle('➜ Command Failed')
        .setColor(Colors.RED)
        .setDescription(Markup.bold('This command is for client operators only.'))
        context.editOrReply({ embed: embed, reference: true })
        return false;
      }
    }

    async run(context: Context, args: ParsedArgs) {
      try {
        context.commandClient.clear()
        let err = false;
        await context.commandClient.addMultipleIn('commands', { subdirectories: true }).catch((...e) => {
            e.forEach(e => new Logger().error(e));
            err = true;
        });
        context.commandClient.commands.forEach(c => new Logger().info(`Reloaded command ${c.name}`))
        const embed = new Embed().setTitle('➜ Reloaded')
        if(!err) {
          embed.setColor(Colors.GREEN)
          embed.setDescription(Markup.bold('Reloaded commands with no errors.'))
        } else {
          embed.setColor(Colors.YELLOW)
          embed.setDescription(Markup.bold('Reloaded commands with errors.'))
        }

        context.editOrReply({ embed: embed, reference: true })
      } catch (err) {
        new Logger().error(err)
      }
    }
}