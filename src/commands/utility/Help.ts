import { CommandClient } from "detritus-client";
import type { Command, Context, ParsedArgs } from "detritus-client/lib/command";
import { Embed, Markup } from "detritus-client/lib/utils";
import { BaseCollection } from "detritus-client/lib/collections";
import { Colors, CommandMetadata, CommandCategories } from "@src/interfaces";
import { BaseCommand } from "@src/commands/BaseCommand";

interface SpecialArgs extends ParsedArgs {
    command?: string
}

export = class Help extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            label: 'command',
            type: 'STRING',
            name: 'help',
            metadata: {
                category: 'UTILITY',
                description: 'Get all commands or info about a command.'
            }
        })
    }

    run(context: Context, args: SpecialArgs) {
        if(!args.command) {
            let categories = new BaseCollection<keyof typeof CommandCategories, BaseCollection<string, Command<any>>>();
            let commands = context.commandClient.commands;
            commands.forEach(cmd => {
                let category = categories.get((cmd.metadata as CommandMetadata).category);
                if(category) {
                    category.set(cmd.name, cmd);
                } else {
                    categories.set((cmd.metadata as CommandMetadata).category, new BaseCollection<string, Command<any>>().set(cmd.name, cmd));
                }
            });
            let embed = new Embed()
            .setTitle('➜ Help')
            .setColor(Colors.ORANGE);
            
            categories.forEach((v, k) => {
                embed.addField(`⤥ ${CommandCategories[k]}`, v.map(c => `⤤ ${Markup.codestring(c.name)}`).join('\n'), true);
            })

            return context.editOrReply({ embed: embed, reference: true });
        } else {
            let command = context.commandClient.commands.find(c => c.name.toLowerCase() === args.command?.toLowerCase());
            if(!command) return;

            let embed = new Embed()
            .setTitle(`➜ Command Help`)
            .setColor(Colors.ORANGE);

            if(command.metadata) {
                let data = '';
                let metadata = command.metadata as CommandMetadata
                if(metadata.adminOnly) data += `⤤ Admin-only: ${Markup.codestring(metadata.adminOnly.toString())}\n`;
                if(metadata.category) data += `⤤ Category: ${Markup.codestring(CommandCategories[metadata.category])}\n`;
                if(metadata.description) data += `⤤ Description: ${Markup.codestring(metadata.description)}\n`;
                if(metadata.examples) data += `⤤ Examples:\n${Markup.codeblock(metadata.examples.map(e => `  - ${e.toLowerCase()}`).join('\n'))}\n`;
                if(metadata.nsfw) data += `⤤ NSFW: ${Markup.codestring(metadata.nsfw.toString())}\n`;
                if(metadata.usage) data += `⤤ Usage: ${Markup.codestring(metadata.usage)}\n`;
                embed.addField('⤥ Metadata', data, true);
            }

            let data2 = '';
            if(command.aliases && command.aliases.length) data2 += `⤤ Aliases:\n${Markup.codeblock(command.aliases.map(e => `  - ${e.toLowerCase()}`).join('\n'))}\n`;
            embed.addField(`⤥ Info`, data2.length > 0 ? data2 : '⤤ No info.')
            return context.editOrReply({ embed: embed, reference: true });
        }
    }
}