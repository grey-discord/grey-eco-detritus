import { CommandClient } from "detritus-client";
import type { Context, ParsedArgs } from "detritus-client/lib/command";
import { Embed, Markup } from "detritus-client/lib/utils";
import { BaseCommand } from "@src/commands/BaseCommand";
import { Colors } from "@src/interfaces";

interface SpecialArgs extends ParsedArgs {
    title: string,
    desc?: string,
    footer?: string,
    color?: number
}

export = class Eval extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'embed',
            metadata: {
                category: 'UTILITY',
                adminOnly: true,
                description: 'Creates an embed.',
                examples: ['ge?embed --title lol --desc testing done']
            },
            args: [
                {
                    name: 'title',
                    prefix: '--',
                    type: 'STRING',
                    consume: true
                },
                {
                    name: 'desc',
                    prefix: '--',
                    type: 'STRING',
                    consume: true
                },                              
                {
                    name: 'footer',
                    prefix: '--',
                    type: 'STRING',
                    consume: true
                },                
                {
                    name: 'color',
                    prefix: '--',
                    type: 'NUMBER',
                },
            ]
        })
    }

    onBeforeRun(_context: Context, args: SpecialArgs) {
        console.log(args)
        if(args.color) return !!args.title && !isNaN(args.color)
        else return !!args.title 
    }

    onCancelRun(context: Context, args: SpecialArgs) {
        if(!args.title) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Please enter a valid title.')).setTimestamp(), reference: true })
            return false;
        }
        if(args.color && isNaN(args.color)) {
            context.editOrReply({ embed: new Embed()
            .setTitle('➜ Command Failed')
            .setColor(Colors.RED)
            .setDescription(Markup.bold('Please enter a valid title.')).setTimestamp(), reference: true })
            return false;
        }
    }

    run(context: Context, args: SpecialArgs) {
        let embed = new Embed().setColor(Colors.ORANGE)
        if(args.title) embed.setTitle(args.title)
        if(args.desc) embed.setDescription(args.desc)
        if(args.footer) embed.setFooter(args.footer)
        if(args.color) embed.setColor(args.color)
        context.editOrReply({ embed: embed, reference: true })
    }
}