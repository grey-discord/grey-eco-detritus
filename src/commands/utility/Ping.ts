import { CommandClient } from "detritus-client";
import type { Context, ParsedArgs } from "detritus-client/lib/command";
import { Embed, Markup } from "detritus-client/lib/utils";
import { BaseCommand } from "@src/commands/BaseCommand";
import { Colors } from "@src/interfaces";
export = class Ping extends BaseCommand {
    constructor(client: CommandClient) {
        super(client, {
            name: 'ping',
            aliases: ['ms'],
            metadata: {
                category: 'UTILITY',
                description: 'Get the bot latency.'
            }
        })
    }

    async run(context: Context, _args: ParsedArgs) {
        let ping = (await context.client.ping())
        return context.editOrReply({ embed: new Embed()
            .setTitle('➜ Latency/Ping')
            .setColor(Colors.ORANGE)
            .setDescription(
            `${Markup.bold('HTTP/API Latency:')} ${ping.rest.toString()}ms
            ${Markup.bold('WS Latency:')} ${ping.gateway.toString()}ms`
        ), reference: true })
    }
}