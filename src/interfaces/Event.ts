import type { CommandClient } from "detritus-client";

export interface Event { 
    default: { 
        name: string,
        cluster?: boolean,
        execute(payload: any, attachment: CommandClient): void 
    };
}