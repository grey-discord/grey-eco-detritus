import type { AcceptedDBKeys, AcceptedDBValues } from "@src/interfaces/economy";
import type { ShopItem } from "@src/interfaces"
import type { Snowflake } from "detritus-client/lib/constants";
import type { BaseCollection } from "detritus-client/lib/collections";
import Enmap from "enmap";

export interface Settings {
    collections: {
        sniped: BaseCollection<Snowflake, any>
    }
    config?: {
        operators: Array<Snowflake>,
        defaultCooldown?: number
    },
    databases?: {
        economy: Enmap<AcceptedDBKeys, AcceptedDBValues>,
        shop: Enmap<string, ShopItem>
    }
}