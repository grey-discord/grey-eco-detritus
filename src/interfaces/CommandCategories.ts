export enum CommandCategories {
    'ECONOMY' = 'Economy',
    'UTILITY' = 'Utility',
    'SHOP' = 'Shop',
    'FUN' = 'Fun'
}