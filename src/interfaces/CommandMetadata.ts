import { CommandCategories } from "@src/interfaces/CommandCategories";

export interface CommandMetadata {
    category: keyof typeof CommandCategories;
    description?: string;
    examples?: string[];
    nsfw?: boolean;
    usage?: string;
    adminOnly?: boolean;
}