export enum Constants {
    "coin_name" = "Grecoin",
    "coin_name_lowercase" = "grecoin",
    "coin_emoji" = "💰"
}