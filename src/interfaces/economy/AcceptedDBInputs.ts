import type { Snowflake } from "detritus-client/lib/constants";
import type { EconomyProfile, EconomyWork } from "@src/interfaces/economy";
import { EconomyJobs } from "@src/interfaces/economy";

export type AcceptedDBKeys = `${Snowflake}_${Snowflake}` | `${Snowflake}_${Snowflake}.balance` | `${Snowflake}_${Snowflake}.work` | `${Snowflake}_${Snowflake}.daily`
export type AcceptedDBValues = EconomyProfile | EconomyWork | keyof typeof EconomyJobs | number | Date | null | undefined