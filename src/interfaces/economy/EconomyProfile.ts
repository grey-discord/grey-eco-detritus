import { EconomyWork, ShopItem } from "@src/interfaces/economy";

export interface EconomyProfile {
    balance: number,
    daily?: Date | number,
    work?: EconomyWork,
    inventory?: Array<ShopItem | Array<ShopItem>>
}