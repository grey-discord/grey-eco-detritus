import type { ComponentEmojiData } from "detritus-client/lib/utils";

export interface ShopItem {
    name: string,
    price: number,
    emoji: ComponentEmojiData,
    description?: string
}