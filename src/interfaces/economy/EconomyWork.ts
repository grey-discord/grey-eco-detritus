import type { EconomyJobs } from "@src/interfaces/economy/index";

export interface EconomyWork {
    title: keyof typeof EconomyJobs,
    salary: 1000 | 1500 | 2000 | 2500 | 3000 | 3500 | 4000 | 4500 | 5000 | 6000,
    date: Date | number
}