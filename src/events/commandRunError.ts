import { CommandEvents } from "detritus-client/lib/command";
import { Logger } from "@src/classes/Logger";

export default {
    name: 'commandRunError',
    execute: async (payload: CommandEvents.CommandRunError) => {
        new Logger().error(payload.error, payload.error.stack)
    }
}