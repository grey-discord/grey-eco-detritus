import { Logger } from "@src/classes/Logger";
import { CommandClient, GatewayClientEvents } from "detritus-client";

export default {
    name: 'gatewayReady',
    cluster: true,
    execute: async (_payload: GatewayClientEvents.GatewayReady, client: CommandClient) => {
        let err = false;
        await client.addMultipleIn('commands', { subdirectories: true }).catch((...e) => {
            e.forEach(e => new Logger().error(e));
            err = true;
        });
        client.commands.forEach(c => new Logger().info(`Loaded command ${c.name}`))
    }
}