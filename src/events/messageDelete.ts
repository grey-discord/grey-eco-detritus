import { CommandClient, GatewayClientEvents } from "detritus-client";
import type { Snowflake } from "detritus-client/lib/constants";

export default {
    name: 'messageDelete',
    cluster: true,
    execute: async (payload: GatewayClientEvents.MessageDelete, client: CommandClient) => {
        let data:{
            message: string,
            author: Snowflake,
            attachment?: string,
            date: number
        } = {
            message: payload.message!.content,
            author: payload.message!.author.id,
            date: Date.now()
        }

        if(payload.message?.hasAttachment && payload.message.attachments) {
            data.attachment = payload.message.attachments.first()!.proxyUrl!
        }
        client.settings.collections.sniped.set(payload.channelId, data)
    }
}