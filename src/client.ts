import type { Settings } from '@src/interfaces';
import type { AcceptedDBKeys, AcceptedDBValues } from '@src/interfaces/economy';
import type { ShopItem } from "@src/interfaces"
import { Handler, Logger } from '@src/classes';

import type { Context } from 'detritus-client/lib/command';
import type { Snowflake } from "detritus-client/lib/constants";
import { BaseCollection } from "detritus-client/lib/collections";
import { CommandClient } from 'detritus-client';
import { GatewayIntents } from 'detritus-client-socket/lib/constants';

import { config as dot } from 'dotenv';
dot({ path: process.cwd() + "/src/.env" })

import { readdirSync } from 'fs';
import path from 'path';
import Enmap from 'enmap';

const prefixes = ['ge?']

declare module 'detritus-client/lib/commandclient' {
  interface CommandClient {
    settings: Settings
  }
}

const client = new CommandClient(process.env.TOKEN!, {
    useClusterClient: true,
    gateway: {
      presence: {
        activity: {
          type: 0,
          name: 'ge?help'
        },
        status: 'idle'
      },
      intents: [
        GatewayIntents.GUILDS,
        GatewayIntents.GUILD_MEMBERS,
        GatewayIntents.GUILD_MESSAGES,
        GatewayIntents.GUILD_PRESENCES,
        GatewayIntents.GUILD_MESSAGE_REACTIONS,
      ],
    },
    prefixes,
    onPrefixCheck: (_payload: Context) => {
      const prefix = prefixes;
  
      return prefix;
    },
});

client.settings = {
  collections: {
    sniped: new BaseCollection<Snowflake, any>()
  },
  config: {
    operators: ['233252032662863872', '775405014947594241']
  },
  databases: {
    economy: new Enmap<AcceptedDBKeys, AcceptedDBValues>({ name: 'economy' }),
    shop: new Enmap<string, ShopItem>({ name: 'shop' })
  }
};

(async () => {
    await new Handler().loadEvents(readdirSync(path.join(__dirname, './events/')), true)
    await client.run()
    new Logger().info('Loaded client')
})();


export { client };

