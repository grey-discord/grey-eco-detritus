import { client } from "@src/client";
import { Logger } from "@src/classes/Logger";
import type { Event } from "@src/interfaces";

export class Handler {
    async loadEvents(files: Array<string>, log?: boolean) {
        for(const file of files) {
            const ret:Event = await import('../events/' + file)
            try {
                client.on(ret.default.name, payload => ret.default.execute(payload, client));
                if(ret.default.cluster && ret.default.cluster === true) client.client.on(ret.default.name, payload => ret.default.execute(payload, client));
                if(log && log === true) new Logger().info(`Loaded event ${ret.default.name}`)
            }
            catch (error) {
                console.error(error.stack);
            }
        }
    }
}