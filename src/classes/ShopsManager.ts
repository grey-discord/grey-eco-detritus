import { CommandClient, ShardClient } from "detritus-client";
import { Context } from "detritus-client/lib/command";
import { Message } from "detritus-client/lib/structures";
import type { ComponentEmojiData } from "detritus-client/lib/utils";

export class ShopsManager {
    protected context = new Context(new Message(new ShardClient(process.env.TOKEN!)), null, new CommandClient(process.env.TOKEN!))
    constructor(context: Context) {
        this.context = context
    }

    genID(name: string) {
        return name.toLowerCase().replace(/\s/gm,'-').replace(/[!@#$%^&*()]/gm, '_')
    }

    newItem(name: string, price: number, emoji?: ComponentEmojiData, desc?: string) {
        if(!emoji) emoji = '➕';
        if(!desc) desc = 'No description provided.';
        this.context.commandClient.settings.databases!.shop.set(this.genID(name), {
            name: name,
            price: price,
            emoji: emoji,
            description: desc
        });
        return this.context.commandClient.settings.databases!.shop.get(this.genID(name))!;
    }

    getItem(id: string) {
        let item = this.context.commandClient.settings.databases?.shop.get(id.toLowerCase())
        if(item === null || item === undefined) return null;
        return item;
    }

    removeItem(name: string) {
        if(this.getItem(this.getID(name)!) === null) return false;
        this.context.commandClient.settings.databases?.shop.delete(this.getID(name)!)
        return true;
    }

    hasItemWithName(name: string) {
        if(this.getItem(this.getID(name)!) === null) return false;
        return this.context.commandClient.settings.databases?.shop.has(this.getID(name)!)!
    }

    hasItemWithID(id: string) {
        if(this.getItem(id) === null) return false;
        return this.context.commandClient.settings.databases?.shop.has(id)!
    }

    getID(name: string) {
        return this.context.commandClient.settings.databases?.shop.findKey(i => i.name.toLowerCase() === name.toLowerCase())
    }

    setEmoji(id: string, emoji: string) {
        if(!this.hasItemWithID(this.getID(id)!)) return false;
        this.context.commandClient.settings.databases?.shop.set(this.getID(id)!, emoji, "emoji")
    }   

    setDescription(id: string, description: string) {
        if(!this.hasItemWithID(this.getID(id)!)) return false;
        this.context.commandClient.settings.databases?.shop.set(this.getID(id)!, description, "emoji")
    }   

    collection() {
        return this.context.commandClient.settings.databases?.shop.array()
    }
}