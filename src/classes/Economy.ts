import { CommandClient, ShardClient } from "detritus-client";
import type { Snowflake } from "detritus-client/lib/constants";
import { Context } from "detritus-client/lib/command";
import { Message } from "detritus-client/lib/structures";
import Enmap from "enmap";
import type { AcceptedDBKeys, AcceptedDBValues, EconomyProfile } from "@src/interfaces/economy";

export class Economy {
    protected economy = new Enmap<AcceptedDBKeys, AcceptedDBValues>({ name: 'economy' })
    protected context = new Context(new Message(new ShardClient(process.env.TOKEN!)), null, new CommandClient(process.env.TOKEN!))
    constructor(context: Context) {
        this.context = context
        this.economy = this.context.commandClient.settings.databases?.economy!

    }

    setProfile(guildID: Snowflake, userID: Snowflake) {
        if(!(<ShardClient>this.context.client).users.some(u => u.id === userID)) throw new Error('User does not exist.');
        if(!(<ShardClient>this.context.client).guilds.some(g => g.id === guildID)) throw new Error('Guild does not exist');
        return this.economy.ensure(`${guildID}_${userID}`, <EconomyProfile>{
            balance: <number>0,
            work: <any>{},
            inventory: []
        })
    }

    setBalance(guildID: Snowflake, userID: Snowflake, amount: number) {
        this.setProfile(guildID, userID);
        this.economy.set(`${guildID}_${userID}`, <number>amount, "balance");
        return this.economy.get(`${guildID}_${userID}`, "balance");
    }

    getBalance(guildID: Snowflake, userID: Snowflake) {
        this.setProfile(guildID, userID);
        return <number>this.economy.get(`${guildID}_${userID}`, "balance") || 0;
    }
}