import chalk from "chalk";

export class Logger {
    returnISOString() {
        return chalk.grey(`[${new Date().toISOString()}]`)
    }

    info(...args: any[]) {
        console.log(`${this.returnISOString()} ${chalk.greenBright('[INFO]')} ${chalk.bold.dim(args)}`)
    }

    warn(...args: any[]) {
        console.log(`${this.returnISOString()} ${chalk.yellowBright('[WARN]')} ${chalk.bold.dim(args)}`)
    }

    error(...args: any[]) {
        console.log(`${this.returnISOString()} ${chalk.redBright('[ERROR]')} ${chalk.bold.dim(args)}`)
    }
}