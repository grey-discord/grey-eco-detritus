FROM node:14-alpine

WORKDIR /opt/app

ENV PORT=80

RUN apk --update add make python gcc g++

RUN sudo npm install -g --save ts-node typescript 

RUN npm install @types/node

RUN npm install -g nodemon
RUN npm install -g pm2

RUN echo 'set -e' > /boot.sh # this is the script which will run on start

RUN echo 'nb=`cat package.json | grep start | wc -l` && if test "$nb" = "0" ; then echo "*** Boot issue: No start command found in your package.json in the scripts. See https://docs.npmjs.com/cli/start" ; exit 1 ; fi' >> /boot.sh

COPY package*.json ./
RUN npm install --production

COPY . .

RUN mkdir -p logs

CMD sh /boot.sh && pm2-runtime --output logs/out.log --error logs/error.log start npm -- start
